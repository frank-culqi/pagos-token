import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/crear-token',
    name: 'CrearToken',
    component: () => import(/* webpackChunkName: "about" */ '../components/CrearToken.vue')
  },
  {
    path: '/consultar-token',
    name: 'ConsultarToken',
    component: () => import(/* webpackChunkName: "about" */ '../components/ConsultarToken.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
